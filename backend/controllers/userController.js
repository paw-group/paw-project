const User = require("../models/user/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const saltRounds = 10;

var userController = {};

userController.createUser = async function (req, res, next) {
  //Caso for um utilizador novo
  if (req.userRole == undefined) {
    //Nao deixa definir role
    if (req.body.role != undefined)
      return res
        .status(401)
        .send({ auth: false, message: "Without permission" });
    //Não deixa definir estado
    if (req.body.state != undefined)
      return res
        .status(401)
        .send({ auth: false, message: "Without permission" });
    //Nao deixa criar conta com o login feito a não ser o admin
  } else if (req.userRole < process.env.ADMINROLE)
    return res.status(401).send({ auth: false, message: "Without permission" });

  if (req.body.role == undefined) req.body.role = 0;

  try {
    //Encriptação da password
    req.body.password = bcrypt.hashSync(req.body.password, saltRounds);
    var user = new User(req.body);

    //Gravar na base de dados o User
    user.save(function (err) {
      err ? next(err) : res.json(user);
    });
  } catch (err) {
    err = new Error();
    err.message = "Form Not Acceptable";
    err.status = 406;
    next(err);
  }
};

userController.updateUser = function (req, res, next) {
  //Caso nao for o admin
  if (req.userRole < process.env.ADMINROLE) {
    //Caso for o proprio utilizador
    if (req.params.userId == req.userId) {
      //Nao deixa mudar o estado
      if (req.body.state != undefined)
        return res
          .status(401)
          .send({ auth: false, message: "Without permission" });
      //Nao deixa mudar o nome de utilizador
      if (req.body.userName != undefined)
        return res
          .status(401)
          .send({ auth: false, message: "Without permission" });
      //Nao deixa mudar a role
      if (req.body.role != undefined)
        return res
          .status(401)
          .send({ auth: false, message: "Without permission" });
      //Caso for um update do tecnico de laboratorio
    } else if (req.userRole > process.env.USERROLE) {
      //Nao deixa mudar o nome
      if (req.body.name != undefined)
        return res
          .status(401)
          .send({ auth: false, message: "Without permission" });
      //Nao deixa mudar o nome de utilizador
      if (req.body.userName != undefined)
        return res
          .status(401)
          .send({ auth: false, message: "Without permission" });
      //Nao deixa mudar password
      if (req.body.password != undefined)
        return res
          .status(401)
          .send({ auth: false, message: "Without permission" });
      //Nao deixa mudar o numero de telemovel
      if (req.body.phoneNumber != undefined)
        return res
          .status(401)
          .send({ auth: false, message: "Without permission" });
      //Nao deixa mudar a role
      if (req.body.role != undefined)
        return res
          .status(401)
          .send({ auth: false, message: "Without permission" });
    }
  }

  //Verificação do número de telemovél
  if (req.body.phoneNumber != undefined) {
    let phoneNumber = parseInt(req.body.phoneNumber)
    if (phoneNumber < 910000000 || phoneNumber > 969999999) {
      return res
        .status(406)
        .send({ auth: false, message: "Form Not Acceptable" });
    }
  }

  //Caso existir uma password nova ela é encriptada
  if (req.body.password != undefined)
    req.body.password = bcrypt.hashSync(req.body.password, saltRounds);

  //Para atualizar a data de update
  req.body.updated_at = Date.now();

  User.findByIdAndUpdate(req.params.userId, req.body, { new: true }, function (
    err,
    user
  ) {
    err ? next(err) : res.json(user);
  });
};

userController.deleteUser = function (req, res, next) {
  //Só deixa um admin eliminar um user ou o proprio ser o proprio a fazer isso
  if (req.userRole < process.env.ADMINROLE && req.params.userId != req.userId)
    return res.status(401).send({ auth: false, message: "Without permission" });

  User.findByIdAndDelete(req.params.userId, req.body, function (err, user) {
    err ? next(err) : res.json(user);
  });
};

userController.getAllUsers = function (req, res, next) {
  const filters = {};

  //Adiciona filtro por nome
  if (req.query.name != undefined) filters.name = req.query.name;

  //Adiciona filtro por numero de telemovel
  if (req.query.phoneNumber != undefined)
    filters.phoneNumber = req.query.phoneNumber;

  //Adiciona filtro por estado do utilizador
  if (req.query.state != undefined) filters.state = req.query.state;

  User.find(filters, function (err, users) {
    err ? next(err) : res.json(users);
  });
};

userController.getUserById = function (req, res, next) {
  //Só deixa um superior do user ver um perfil ou o proprio
  if (
    req.userRole < process.env.LABTECHNICROLE &&
    req.params.userId != req.userId
  )
    return res.status(401).send({ auth: false, message: "Without permission" });

  User.findById(req.params.userId, function (err, user) {
    err ? next(err) : res.json(user);
  });
};

userController.login = function (req, res, next) {
  User.find({ userName: req.body.userName }, function (err, user) {
    if (err) next(err);
    else if (user.length < 1)
      res.status(401).json({ auth: false, status: "Authentication Error" });
    else {
      bcrypt.compare(req.body.password, user[0].password, function (
        err,
        result
      ) {
        if (err) next(err);
        else if (result) {
          const token = jwt.sign(
            { id: user[0]._id, name: user[0].name, role: user[0].role },
            process.env.SECRET,
            {
              expiresIn: 600000,
            }
          ); //expires in 5min
          console.log(`Fez login e gerou o token: ${token}`);

          res.cookie("session", token, {
            expires: new Date(Date.now() + 600000),
            htppOnly: true,
          });

          let id = user[0]._id.toString();

          res.cookie("user", id, {
            expires: new Date(Date.now() + 600000),
            htppOnly: true,
          });

          return res.status(200).json({ auth: true, token: token });
        } else {
          res.status(401).json({ auth: false, status: "Authentication Error" });
        }
      });
    }
  });
};

userController.logout = function (req, res, next) {
  try {
    res.clearCookie("session");
    res.clearCookie("user");
    return res.status(200).json({ auth: false, message: "Logout with sucess" });
  } catch (err) {
    next(err);
  }
};

userController.refresh = function (req, res, next) {
  const token = jwt.sign(
    { id: req.userId, name: req.name, role: req.userRole },
    process.env.SECRET,
    {
      expiresIn: 600000,
    }
  ); //expires in 5min

  console.log(`Fez login e gerou o token: ${token}`);

  res.cookie("session", token, {
    expires: new Date(Date.now() + 600000),
    htppOnly: true,
  });

  res.cookie("user", req.userId, {
    expires: new Date(Date.now() + 600000),
    htppOnly: true,
  });

  return res.status(200).json({ auth: true, token: token });
};

module.exports = userController;
