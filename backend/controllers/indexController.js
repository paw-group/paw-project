const Request = require("../models/request/request");
const User = require("../models/user/user");
const requestState = require("../models/request/state.json");
const userState = require("../models/user/state.json");

module.exports = async function (req, res, next) {
  var json = {};
  json.users = {};
  json.AllRequests = {};
  json.requests = new Array();
  AllRequestsCopy = {};

  await User.countDocuments(
    {
      state: userState.Infected,
    },
    function (err, count) {
      if (err) next(err);
      else {
        json.users.Infected = count;
      }
    }
  );

  await User.countDocuments(
    {
      state: userState.Not_Infected,
    },
    function (err, count) {
      if (err) next(err);
      else {
        json.users.Not_Infected = count;
      }
    }
  );

  await User.countDocuments(
    {
      state: userState.Suspect,
    },
    function (err, count) {
      if (err) next(err);
      else {
        json.users.Suspect = count;
      }
    }
  );

  if (req.userRole > process.env.USERROLE) {
    await Request.countDocuments(
      {
        state: requestState.Not_Schedule,
      },
      function (err, count) {
        if (err) next(err);
        else {
          json.AllRequests.Not_Schedule = count;
        }
      }
    );

    await Request.countDocuments(
      {
        state: requestState.Schedule,
      },
      function (err, count) {
        if (err) next(err);
        else {
          json.AllRequests.Schedule = count;
        }
      }
    );
    await Request.countDocuments(
      {
        state: requestState.Done,
      },
      function (err, count) {
        if (err) next(err);
        else {
          json.AllRequests.Done = count;
        }
      }
    );

    await Request.countDocuments(
      {
        state: requestState.Missed,
      },
      function (err, count) {
        if (err) next(err);
        else {
          json.AllRequests.Missed = count;
        }
      }
    );
  }
  await Request.countDocuments(
    {
      state: requestState.In_Lab,
    },
    function (err, count) {
      if (err) next(err);
      else {
        json.AllRequests.In_Lab = count;
      }
    }
  );

  await Request.countDocuments(
    {
      result: true,
    },
    function (err, count) {
      if (err) next(err);
      else {
        json.AllRequests.Positive = count;
        AllRequestsCopy.Positive = count;
      }
    }
  );

  await Request.countDocuments(
    {
      result: false,
    },
    function (err, count) {
      if (err) next(err);
      else {
        json.AllRequests.Negative = count;
        AllRequestsCopy.Negative = count;
      }
    }
  );

  const DAY = 86400000;
  var dategte = new Date(Date.now());
  dategte.setUTCHours(0);
  dategte.setMinutes(0);
  dategte.setSeconds(0);
  dategte.setMilliseconds(0);
  var datelte = new Date(Date.parse(dategte) + DAY);

  //Informaçao por dia até 30 dias antes
  for (let i = 0; i < 30; i++) {
    var requests = {};

    await Request.countDocuments(
      {
        TestDate: {
          $gte: dategte,
          $lte: datelte,
        },
        result: true,
      },
      function (err, count) {
        if (err) next(err);
        else {
          requests.Positive = count;
          requests.PositiveUntilNow = AllRequestsCopy.Positive;
          AllRequestsCopy.Positive -= count;
        }
      }
    );

    await Request.countDocuments(
      {
        TestDate: {
          $gte: dategte,
          $lte: datelte,
        },
        result: false,
      },
      function (err, count) {
        if (err) next(err);
        else {
          requests.Negative = count;
          requests.NegativeUntilNow = AllRequestsCopy.Negative;
          AllRequestsCopy.Negative -= count;
        }
      }
    );
    requests.date = dategte;
    datelte = new Date(Date.parse(datelte) - DAY);
    dategte = new Date(Date.parse(dategte) - DAY);
    requestState.date = json.requests.push(requests);
  }
  return res.json(json);
};
