const Request = require("../models/request/request");
const User = require("../models/user/user");
const path = require('path')

var requestController = {};

requestController.createRequest = function (req, res, next) {
  if (req.userRole < process.env.ADMINROLE) {
    //Verificação de atributos que não podem ser criados no inicio
    if (req.body.state != undefined)
      return res
        .status(401)
        .json({ auth: false, message: "Without permission" });
    if (req.body.TestDate != undefined)
      return res
        .status(401)
        .json({ auth: false, message: "Without permission" });
    if (req.body.resultFile != undefined)
      return res
        .status(401)
        .json({ auth: false, message: "Without permission" });
    if (req.body.result != undefined)
      return res
        .status(401)
        .json({ auth: false, message: "Without permission" });

    //Obriga que o userId do request seja igual ao id do user logado e isto impossibilita
    //um technico criar um request para uma outra pessoa, obriga o proprio a criar
    req.body.userId = req.userId;
  } else {
    if (req.body.userId == undefined) req.body.userId = req.userId;
  }

  if (process.env.AUTOSCHEDULE == "true") next();
  else {
    const request = new Request(req.body);
    request.save((err) => {
      err ? next(err) : res.json(request);
    });
  }
};

requestController.updateRequest = function (req, res, next) {
  if (req.userRole < process.env.ADMINROLE) {
    if (req.userRole < process.env.LABTECHNICROLE) {
      //Caso seja o dono do request a utualizar ele não pode tentar atualizar os seguintes campos
      if (req.request.userId == req.userId) {
        //Nao pode atualizar o state
        if (req.body.state != undefined)
          return res
            .status(401)
            .json({ auth: false, message: "Without permission" });
        //Nao pode atualizar o TestDate
        if (req.body.TestDate != undefined)
          return res
            .status(401)
            .json({ auth: false, message: "Without permission" });
        //Nao pode atualizar o resultFile
        if (req.body.resultFile != undefined)
          return res
            .status(401)
            .json({ auth: false, message: "Without permission" });
        //Nao pode atualizar o result
        if (req.body.result != undefined)
          return res
            .status(401)
            .json({ auth: false, message: "Without permission" });
      }
    }
    //Não pode ser alterado o userId presente no request
    if (req.body.userId != undefined)
      return res
        .status(401)
        .json({ auth: false, message: "Without permission" });
  }

  if (req.body.TestDate != undefined) {
    let date = new Date(req.body.TestDate);

    if (date.getHours() > 19 || date.getHours() < 8) {
      return res.status(406).json({ message: "Not Acceptable" });
    }
    if (date.getMinutes() % 10 != 0) {
      return res.status(406).json({ message: "Not Acceptable" });
    }
  }

  //Para atualizar a data de update
  req.body.updated_at = Date.now();

  //Atualizar o estado do pedido para concluido
  if (req.body.result != undefined) {
    req.body.state = 3;
  }

  //Manter integridade da base de dados
  if (req.body.resultFile == "") {
    req.body.resultFile = undefined;
  }

  Request.findByIdAndUpdate(
    req.params.requestId,
    req.body,
    { new: true },
    function (err, request) {
      if (err) {
        next(err);
      } else {
        //Atualizar o estado do paciente
        if (req.body.result == "true") {
          User.findByIdAndUpdate(
            request.userId,
            { state: 2, updated_at: Date.now() },
            function (err) {
              if (err) next(err);
            }
          );
        }
        res.json(request);
      }
    }
  );

  if (process.env.AUTOSCHEDULE == "true") next();
};

requestController.getFile = function (req, res, next) {
  //Verifica se o ficheiro existe, se nao existir dá um erro
  if (req.request.resultFile == undefined)
    return res.status(404).json({ message: "File not exist" });
  
  res.sendFile(
    path.join(
      __dirname.split("controllers")[0] + 'pdf_files/' + req.request.resultFile
    ),
    (err) => {
      if (err) next(err);
    }
  );
};

requestController.deleteRequest = function (req, res, next) {
  //Só uma pessoa com credenciais superiores ao user é que pode eliminar um request ou o proprio
  //dono do request
  if (
    req.userRole < process.env.LABTECHNICROLE &&
    req.request.userId != req.userId
  )
    return res.status(401).json({ auth: false, message: "Without permission" });

  req.request.remove(function (err) {
    err ? next(err) : res.json(req.request);
  });
};

requestController.getAllRequests = function (req, res, next) {
  const filters = {};

  //Adiciona filtro por risk group
  if (req.query.riskGroup != undefined) filters.riskGroup = req.query.riskGroup;

  //Adiciona filtro por estado do pedido
  if (req.query.state != undefined) filters.state = req.query.state;

  //Adiciona filtro por result
  if (req.query.result != undefined) filters.result = req.query.result;

  //Adiciona filtro por utilizadores redirecionados pela saude 24 ou outros...
  if (req.query.redirected != undefined)
    filters.redirected = req.query.redirected;

  //Adiciona filtro por Data, este eu testei e funciona, exprimentado por Hugo
  if (req.query.TestDate != undefined) {
    console.log(req.query.TestDate);
    const DAY = 86400000;
    var dategte = new Date(req.query.TestDate);
    dategte.setUTCHours(0);
    dategte.setMinutes(0);
    dategte.setSeconds(0);
    dategte.setMilliseconds(0);
    var datelte = new Date(Date.parse(dategte) + DAY);
    filters.TestDate = {
      $gte: dategte,
      $lte: datelte,
    };
  }

  //Adiciona filtro por userId
  if (req.query.userId != undefined) filters.userId = req.query.userId;

  //Deixa só o utilizador normal ver as informações dele, não deixa ver de outros utilizadores
  if (req.userRole <= process.env.USERROLE) filters.userId = req.userId;

  Request.find(filters, function (err, requests) {
    err ? next(err) : res.json(requests);
  });
};

requestController.getOneRequest = function (req, res) {
  //Só uma pessoa com credenciais superiores ao user é que pode obter o request pedido ou o proprio
  //dono do request
  if (req.userRole > process.env.USERROLE || req.request.userId == req.userId)
    res.json(req.request);
  else
    return res.status(401).json({ auth: false, message: "Without permission" });
};

requestController.getByIdRequest = function (req, res, next, requestId) {
  Request.findOne({ _id: requestId }, function (err, request) {
    if (err) next(err);
    if (request == null)
      return res.status(404).json({ message: "Null object" });
    else {
      req.request = request;
      next();
    }
  });
};

requestController.autoScheduleMethod = function (req, res, next) {
  if (req.body.AUTOSCHEDULE == true) {
    process.env.AUTOSCHEDULE = true;
  } else if (req.body.AUTOSCHEDULE == false) {
    process.env.AUTOSCHEDULE = false;
  }
  res.send(process.env.AUTOSCHEDULE);
};

module.exports = requestController;
