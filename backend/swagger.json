{
    "swagger": "2.0",
    "info": {
        "version": "2.0.0",
        "title": "Corona Virus API",
        "description": "REST API for a test scheduling system."
    },
    "host": "localhost:5000",
    "basePath": "/api",
    "tags": [
        {
            "name": "Users",
            "description": "API for users in the system."
        },
        {
            "name": "Requests",
            "description": "API for requests in the system."
        },
        {
            "name": "Dashboard",
            "description": "API for Dashboards in the system."
        }
    ],
    "schemes": [
        "http"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "paths": {
        "/user/login": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Login a user.",
                "description": "Logins a user in the system.",
                "parameters": [
                    {
                        "name": "user",
                        "in": "body",
                        "description": "User that we want to log.",
                        "schema": {
                            "$ref": "#/definitions/Login"
                        }
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "User is logged on"
                    },
                    "401": {
                        "description": "Authentication Error"
                    }
                }
            }
        },
        "/user/refresh": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Refresh the existing token.",
                "description": "Refresh the existing token in the system.",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Token refreshed... User continues logged on"
                    },
                    "401": {
                        "description": "Without permission"
                    }
                }
            }
        },
        "/user/logout": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Logout a user.",
                "description": "Logouts a user from the system.",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Logout with sucess"
                    }
                }
            }
        },
        "/user/technicLab": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Create a new user for a technicLab.",
                "description": "Create a new technicLab in the system.",
                "parameters": [
                    {
                        "name": "user",
                        "in": "body",
                        "description": "TechnicLab that we want to create.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "New technicLab is created",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            }
        },
        "/user": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Create a new user.",
                "description": "Create a new user in the system.",
                "parameters": [
                    {
                        "name": "user",
                        "in": "body",
                        "description": "User that we want to create.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "New user is created",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            },
            "get": {
                "tags": [
                    "Users"
                ],
                "summary": "Get all users.",
                "description": "Get all users in the system.",
                "parameters": [
                    {
                        "name": "name",
                        "in": "query",
                        "required": false,
                        "description": "Name of the user/s that we want to find.",
                        "type": "string"
                    },
                    {
                        "name": "phoneNumber",
                        "in": "query",
                        "required": false,
                        "description": "Phone Number of the user/s that we want to find.",
                        "type": "number"
                    },
                    {
                        "name": "state",
                        "in": "query",
                        "required": false,
                        "description": "State of the user/s that we want to find: \n\n0 - Suspeito \n1 - Nao_Infetado \n2 - Infetado",
                        "type": "integer",
                        "enum": [
                            0,
                            1,
                            2
                        ]
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            }
        },
        "/user/{userId}": {
            "parameters": [
                {
                    "name": "userId",
                    "in": "path",
                    "required": true,
                    "description": "ID of the user that we want to find.",
                    "type": "string"
                }
            ],
            "get": {
                "tags": [
                    "Users"
                ],
                "summary": "Get user with the given ID.",
                "description": "Get user with the given ID in the system.",
                "responses": {
                    "200": {
                        "description": "User found",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            },
            "delete": {
                "summary": "Delete user with given ID.",
                "description": "Delete user with given ID from the system.",
                "tags": [
                    "Users"
                ],
                "responses": {
                    "200": {
                        "description": "User is deleted",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            },
            "put": {
                "tags": [
                    "Users"
                ],
                "summary": "Update a User with the given ID.",
                "description": "Update a User with the given ID in the system.",
                "parameters": [
                    {
                        "name": "user",
                        "in": "body",
                        "description": "User with new values of properties.",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User is updated",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                }
            }
        },
        "/request": {
            "post": {
                "tags": [
                    "Requests"
                ],
                "summary": "Create a new request.",
                "description": "Create a new request in the system.",
                "parameters": [
                    {
                        "name": "autoSchedule",
                        "in": "query",
                        "required": false,
                        "description": "Do u want to auto Schedule the test?",
                        "type": "boolean"
                    },
                    {
                        "name": "request",
                        "in": "body",
                        "description": "Request that we want to create.",
                        "schema": {
                            "$ref": "#/definitions/CreateRequest"
                        }
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "New request is created",
                        "schema": {
                            "$ref": "#/definitions/Request"
                        }
                    }
                }
            }
        },
        "/request/file/{requestId}": {
            "parameters": [
                {
                    "name": "requestId",
                    "in": "path",
                    "required": true,
                    "description": "ID of the request that we want to find.",
                    "type": "string"
                }
            ],
            "get": {
                "tags": [
                    "Requests"
                ],
                "summary": "Get the file from a request",
                "description": "Get the file from a request in the system.",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema":{
                            "type":"file"
                        }
                    },
                    "401": {
                        "description": "Without permission"
                    },
                    "404": {
                        "description": "File not exist"
                    },
                    "500": {
                        "description": "Server Error"
                    }
                }
            }
        },
        "/requests": {
            "get": {
                "tags": [
                    "Requests"
                ],
                "summary": "Get all requests.",
                "description": "Get all requests in the system.",
                "parameters": [
                    {
                        "name": "riskGroup",
                        "in": "query",
                        "required": false,
                        "description": "Is the user of the request in a risk group?",
                        "type": "boolean"
                    },
                    {
                        "name": "state",
                        "in": "query",
                        "required": false,
                        "description": "State of the request that we want to find: \n\n0 - Por_Agendar \n1 - Agendado \n2 - Em_Laboratorio \n3 - Concluido",
                        "type": "string",
                        "enum": [
                            0,
                            1,
                            2,
                            3
                        ]
                    },
                    {
                        "name": "result",
                        "in": "query",
                        "required": false,
                        "description": "Is the result of the test true or false?",
                        "type": "boolean"
                    },
                    {
                        "name": "redirected",
                        "in": "query",
                        "required": false,
                        "description": "Is the user of the request redirected from SNS?",
                        "type": "boolean"
                    },
                    {
                        "name": "TestDate",
                        "in": "query",
                        "required": false,
                        "description": "Date of the test if it is already scheduled.",
                        "type": "string",
                        "format": "date-time",
                        "default": "Example: 2017-07-21T17:32:28Z"
                    },
                    {
                        "name": "userId",
                        "in": "query",
                        "required": false,
                        "description": "Id of the user whose request belongs to.",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Request"
                        }
                    }
                }
            }
        },
        "/request/{requestId}": {
            "parameters": [
                {
                    "name": "requestId",
                    "in": "path",
                    "required": true,
                    "description": "ID of the request that we want to find.",
                    "type": "string"
                }
            ],
            "get": {
                "tags": [
                    "Requests"
                ],
                "summary": "Get request with the given ID.",
                "description": "Get request with the given ID in the system.",
                "responses": {
                    "200": {
                        "description": "Request found",
                        "schema": {
                            "$ref": "#/definitions/Request"
                        }
                    }
                }
            },
            "delete": {
                "summary": "Delete request with given ID.",
                "description": "Delete request with given ID from the system.",
                "tags": [
                    "Requests"
                ],
                "responses": {
                    "200": {
                        "description": "Request is deleted",
                        "schema": {
                            "$ref": "#/definitions/Request"
                        }
                    }
                }
            }
        },
        "/request/{requestid}": {
            "put": {
                "tags": [
                    "Requests"
                ],
                "summary": "Update a request with the given ID.",
                "description": "Update a request with the given ID in the system.",
                "parameters": [
                    {
                        "name": "requestid",
                        "in": "path",
                        "required": true,
                        "description": "ID of the request that we want to find.",
                        "type": "string"
                    },
                    {
                        "name": "autoSchedule",
                        "in": "query",
                        "required": false,
                        "description": "Do u want to auto Schedule the test?",
                        "type": "boolean"
                    },
                    {
                        "name": "request",
                        "in": "body",
                        "description": "Request with new values of properties.",
                        "schema": {
                            "$ref": "#/definitions/UpdateRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Request is updated",
                        "schema": {
                            "$ref": "#/definitions/Request"
                        }
                    }
                }
            }
        },
        "/request/autoSchedule": {
            "put": {
                "tags": [
                    "Requests"
                ],
                "summary": "Turns the autoSchedule on or off",
                "description": "Turns the autoSchedule on or off.",
                "parameters": [
                    {
                        "name": "AutoSchedule",
                        "in": "body",
                        "description": "Turns the autoSchedule on or off.",
                        "schema": {
                            "required": [
                                "AUTOSCHEDULE"
                            ],
                            "properties": {
                                "AUTOSCHEDULE": "string",
                                "default": "true"
                            },
                            "example": {
                                "AUTOSCHEDULE": "true"
                            }
                        }
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Success"
                    },
                    "401": {
                        "description": "Without permission"
                    },
                    "406": {
                        "description": "Form not acceptable"
                    },
                    "500": {
                        "description": "Server Error"
                    }
                }
            }
        },
        "/dashboard": {
            "get": {
                "tags": [
                    "Dashboard"
                ],
                "summary": "Number of requests and users.",
                "description": "Information about the state of requests and users.",
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            }
        }
    },
    "definitions": {
        "Login": {
            "required": [
                "userName",
                "password"
            ],
            "properties": {
                "userName": {
                    "type": "string",
                    "uniqueItems": true
                },
                "password": {
                    "type": "string",
                    "format": "password"
                }
            }
        },
        "User": {
            "required": [
                "name",
                "userName",
                "password"
            ],
            "properties": {
                "name": {
                    "type": "string"
                },
                "userName": {
                    "type": "string",
                    "uniqueItems": true
                },
                "password": {
                    "type": "string",
                    "format": "password"
                },
                "phoneNumber": {
                    "type": "integer",
                    "minimum": 910000000,
                    "maximum": 969999999
                }
            }
        },
        "Request": {
            "properties": {
                "userId": {
                    "type": "string"
                },
                "redirected": {
                    "type": "boolean",
                    "default": false
                },
                "riskGroup": {
                    "type": "boolean"
                },
                "resultFile": {
                    "type": "string"
                },
                "result": {
                    "type": "boolean"
                }
            }
        },
        "CreateRequest": {
            "required": [
                "riskGroup"
            ],
            "properties": {
                "redirected": {
                    "type": "boolean",
                    "default": false
                },
                "riskGroup": {
                    "type": "boolean"
                }
            }
        },
        "UpdateRequest": {
            "required": [],
            "properties": {
                "resultFile": {
                    "type": "string"
                },
                "result": {
                    "type": "boolean"
                }
            }
        }
    }
}