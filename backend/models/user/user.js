var mongoose = require("mongoose");
var Schema = mongoose.Schema;
const state = require("./state.json");

var userSchema = new Schema({
  name: { type: String, required: true },
  userName: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  role: {
    //Pode dar erro por causa de ir buscar valores ao process.env
    type: Number,
    min: process.env.USERROLE,
    max: process.env.ADMINROLE,
    default: process.env.USERROLE,
  },
  phoneNumber: { type: Number, min: 910000000, max: 969999999 },
  state: {
    type: Number,
    min: state.Suspect,
    max: state.Infected,
    default: state.Suspect,
  },
  updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("User", userSchema);
