var mongoose = require("mongoose");
var Schema = mongoose.Schema;
const state = require("./state.json");

var requestSchema = new Schema({
  state: {
    type: Number,
    min: state.Not_Schedule,
    max: state.Missed,
    default: state.Not_Schedule,
  },
  userId: { type: String, required: true },
  TestDate: { type: Date },
  redirected: { type: Boolean, default: false },
  riskGroup: { type: Boolean, required: true },
  resultFile: { type: String },
  result: { type: Boolean },
  updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Request", requestSchema);
