const express = require("express");
const router = express.Router();
const requestController = require("../controllers/requestController.js");
const verifyJWT = require("../scripts/verifyJWT.js");
const multer = require("multer");
const autoSchedule = require("../scripts/requestAutoSchedule");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./pdf_files/");
  },
  filename: function (req, file, cb) {
    //Verificação do tipo de ficheiro
    if (
      !file.mimetype.includes("pdf") ||
      req.userRole < process.env.LABTECHNICROLE
    ) {
      var err = new Error();
      err.message = "Wrong file type";
      err.status = 406;
      return cb(err);
    }

    //Guardar o ficheiro com o nome do id do pedido
    req.body.resultFile = req.request._id + ".pdf";
    cb(null, req.request._id + ".pdf");
  },
});

var upload = multer({ storage });

router.post(
  "/request",
  verifyJWT,
  requestController.createRequest,
  autoSchedule.FirstSchedule
);

router.get("/requests", verifyJWT, requestController.getAllRequests);

router.put(
  "/request/autoSchedule",
  (req, res, next) => {
    verifyJWT(req, res, next, process.env.ADMINROLE);
  },
  requestController.autoScheduleMethod
);

router.get("/request/file/:requestId", verifyJWT, requestController.getFile);

router.get("/request/:requestId", verifyJWT, requestController.getOneRequest);

router.put(
  "/request/:requestId",
  verifyJWT,
  upload.single("resultFile"),
  requestController.updateRequest,
  autoSchedule.SecondSchedule
);

router.delete(
  "/request/:requestId",
  verifyJWT,
  requestController.deleteRequest
);

router.param("requestId", requestController.getByIdRequest);

module.exports = router;
