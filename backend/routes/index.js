const express = require('express');
const indexController = require("../controllers/indexController");
var router = express.Router();
const verifyJWT = require("../scripts/verifyJWT.js");


/* GET home page with dashboard. */
router.get('/dashboard', verifyJWT, indexController);

module.exports = router;
