var express = require("express");
var userRouter = express.Router();
var userController = require("../controllers/userController.js");
var verifyJWT = require("../scripts/verifyJWT.js");

userRouter.get(
  "/user/",
  (req, res, next) => {
    verifyJWT(req, res, next, process.env.LABTECHNICROLE);
  },
  userController.getAllUsers
);

userRouter.get("/user/:userId", verifyJWT, userController.getUserById);

userRouter.post("/user/", userController.createUser);

userRouter.post(
  "/user/technicLab",
  (req, res, next) => {
    verifyJWT(req, res, next, process.env.LABTECHNICROLE);
  },
  userController.createUser
);

userRouter.post("/user/login", userController.login);

userRouter.post("/user/logout", userController.logout);

userRouter.post("/user/refresh", verifyJWT, userController.refresh);

userRouter.put("/user/:userId", verifyJWT, userController.updateUser);

userRouter.delete("/user/:userId", verifyJWT, userController.deleteUser);

module.exports = userRouter;
