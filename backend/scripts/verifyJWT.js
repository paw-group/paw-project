const User = require("../models/user/user");
const jwt = require("jsonwebtoken");

module.exports = async function (req, res, next, role = process.env.USERROLE) {
  try {
    var token = req.cookies.session;
    jwt.verify(token, process.env.SECRET, function (err, decoded) {
      if (err) {
        console.log(err);
        return res.status(401).send({ auth: false, message: "Invalid Token!" });
      } else {
        if (decoded.role >= role) {
          req.userRole = decoded.role;
          req.userId = decoded.id;
          req.name = decoded.name; //É para testar no futuro
          next();
        } else
          return res
            .status(401)
            .send({ auth: false, message: "Without permission" });
      }
    });
  } catch (err) {
    console.log(err);
    return res
      .status(401)
      .send({ auth: false, message: "No Authentication token" });
  }
};
