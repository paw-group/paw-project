const mongoose = require("mongoose");
const Request = require("../models/request/request");
const User = require("../models/user/user");

var Schedule = {};

Schedule.FirstSchedule = function (req, res, next) {
  Request.find({ TestDate: { $gt: new Date() } })
    .sort({ TestDate: "asc" })
    .exec(function (err, list) {
      var request = new Request(req.body);

      request.TestDate = findAvailableDay(list);

      //Atualizar o estado do pedido para agendado
      request.state = 1;

      //Guarda o request na base de dados
      request.save((err) => {
        err ? next(err) : res.json(request);
      });
    });
};

Schedule.SecondSchedule = function (req, res, next) {
  //Verifica se existiu update no resultado do teste
  if (req.body.result != undefined && !req.body.result) {
    //Encontra o penultimo teste efetuado pelo utilizador
    Request.find({ userId: req.request.userId })
      .sort({ TestDate: "desc" })
      .limit(2)
      .exec(function (err, lastRequest) {
        var canTest; //Varivel que guarda a informação se já existiram dois testes seguidos com resultado negativo

        //Verifica se o penultimo teste é negativo e caso este o seja agenda um novo teste visto que já e o segundo teste seguido negativo
        if (lastRequest[1] == undefined) {
          canTest = true;
        } else if (lastRequest[1].result) {
          canTest = true;
        }

        //Atualizar o estado do paciente para Nao infetado
        if (!lastRequest[1].result && !lastRequest[0].result) {
          User.findByIdAndUpdate(
            request.userId,
            { state: 1, updated_at: Date.now() },
            function (err) {
              if (err) next(err);
            }
          );
        }

        //Caso estjam reunidas todas as condições, efetua-se o agendamento
        if (canTest && lastRequest[0] != undefined && !lastRequest[0].result) {
          updateSchedule(req, res, next, lastRequest[0]);
        }
      });
  }
};

function updateSchedule(req, res, next, lastRequest) {
  //Alguns problemas com o $gt: (data especifica)
  Request.find({ TestDate: { $gt: new Date() } })
    .sort({ TestDate: "asc" })
    .exec(function (err, list) {
      var newTestDate = findAvailableDay(list);

      var json = {
        userId: lastRequest.userId,
        riskGroup: lastRequest.riskGroup,
        redirected: lastRequest.redirected,
        state: 1, //Atualizar o estado do pedido para agentado
        TestDate: newTestDate,
      };

      var newRequest = new Request(json);

      //Saving the request in the database
      newRequest.save((err) => {
        if (err) next(err);
      });
    });
}

function findAvailableDay(list) {

  var inserted = false;
  var tempDate = new Date();

  const startHour = 8;
  const endHour = 20;
  const testTime = 10; //In minutes
  const minSpace = testTime * 60000
  const maxSpace = (endHour - startHour) * 3600000

  //Data minima para agendar
  var minimunDate = new Date(
    tempDate.getFullYear(),
    tempDate.getMonth(),
    tempDate.getDate() + 2
  );

  //Data que vai ser incrementada
  var currentDate = new Date(
    tempDate.getFullYear(),
    tempDate.getMonth(),
    tempDate.getDate() + 2, startHour, 0
  );

  for (let i = 0; i <= list.length && !inserted; i++) {
    if (list[i] == undefined) {
      inserted = true;
    } else if (list[i].TestDate.getFullYear() == currentDate.getFullYear() && list[i].TestDate.getMonth() == currentDate.getMonth()
      && list[i].TestDate.getDate() == currentDate.getDate()) { //Verifica se os dias são iguais
      if (currentDate > minimunDate && (currentDate.getTime() - list[i].TestDate.getTime() >= minSpace || list[i].TestDate.getTime() - currentDate.getTime() >= minSpace)) { //Verifica se existe espaço para um novo request 
        inserted = true
      } else if(currentDate.getHours() == 19 && currentDate.getMinutes() == 60-testTime) { //Incrementar dia
        currentDate.setDate(currentDate.getDate() + 1);
      } else { //Incrementar minutos
        currentDate.setMinutes(currentDate.getMinutes()+testTime)
      }
    }
  }

  return currentDate;
}

module.exports = Schedule;
