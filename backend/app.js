require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const saltRounds = 10;
const fs = require("fs");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");
const cookieParser = require("cookie-parser");
const cors = require("cors");

//Importing Routes
const indexRouter = require('./routes/index')
const userRouter = require("./routes/userRouter.js");
const requestRouter = require("./routes/requestRouter.js");

mongoose.Promise = global.Promise;

const PORT = process.env.PORT || 3000;
const MONGO_DB_HOST = process.env.MONGO_DB_HOST || "localhost";
const MONGO_DB_PORT = process.env.MONGO_DB_PORT || 27017;
const MONGO_DB_NAME = process.env.MONGO_DB_NAME || "demo";

app.use(express.json());
app.use(cors({ credentials: true, origin: "http://localhost:4200" }));
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

mongoose
  .connect(`mongodb://${MONGO_DB_HOST}:${MONGO_DB_PORT}/${MONGO_DB_NAME}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  })
  .then((mongoose) => {
    console.log(
      `Connected to mongo Host:${MONGO_DB_HOST}, PORT:${MONGO_DB_PORT}, Name:${MONGO_DB_NAME}`
    );
  })
  .catch((err) => {
    console.error(err);
  });

app.listen(PORT, () => {
  console.log(`Server started on http://localhost:${PORT}`);
});

//Inserção da Conta Administrador
const ADMIN = {
  name: "Administrador",
  userName: "admin",
  password: bcrypt.hashSync("admin", saltRounds),
  role: process.env.ADMINROLE,
};
var User = require("./models/user/user");
var user = new User(ADMIN);
user.save(function (err) {
  if (err) console.log(`User ${ADMIN.name} exists in BD`);
  else console.log(`User ${ADMIN.name} returned to standart login information`);
});

//Cria a pasta pdf_files para nao dar erro caso existir um upload para o servidor
// dá erro quando ela já existe ou ocorreu mesmo um erro na sua criação, é preciso atenção
fs.mkdir("./pdf_files", (err) => {
  if (err)
    console.log("Folder pdf_files already exists or has a error! Atencion!");
});

//Configuração das Routes
app.use("/api/",indexRouter);
app.use("/api/", userRouter);
app.use("/api/", requestRouter);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// error handler
app.use(function (err, req, res, next) {
  if (err.code == 11000 || err.name == "ValidationError") {
    err = new Error();
    err.status = 406;
    err.message = "Form Not Acceptable";
  } else if (err.path == "_id" && err.name == "CastError") {
    console.log(err);
    err = new Error();
    err.status = 400;
    err.message = "Id Not Acceptable";
  }
  console.error(err);
  //render the error page
  res.status(err.status || 500).json(err);
});

module.exports = app;
