import { Component, OnInit, Input, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestRequestsService } from '../../services/rest-requests.service';
import { SessionService } from '../../services/session.service';
import { Request } from '../../models/request';
import { User } from 'src/app/models/user';
import { RestUsersService } from 'src/app/services/rest-users.service';
import { NotificationService } from 'src/app/services/notification.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-request-create',
  templateUrl: './request-create.component.html',
  styleUrls: ['./request-create.component.css'],
})
export class RequestCreateComponent implements OnInit {
  @Input() request: Request = new Request();
  users: any = [];
  user: User;
  userSelected: String;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public rest: RestRequestsService,
    private route: ActivatedRoute,
    private router: Router,
    public session: SessionService,
    public restUsers: RestUsersService,
    public notification: NotificationService,
    public dialogRef: MatDialogRef<User>
  ) {
    this.user = new User();
  }

  ngOnInit(): void {
    //Guarda a sessão atual
    this.session.me().subscribe((user: User) => {
      this.request.redirected = false;
      this.request.riskGroup = false;
      this.userSelected = user._id;
      this.user = user;

      //Guarda todos os utilizadores
      if (this.user.role != 0) {
        this.restUsers.getUsers().subscribe((data: {}) => {
          this.users = data;
        });
      }
    });

  }

  //Cria um novo pedido
  createRequest() {
    this.request.userId = this.userSelected;
    this.rest.createRequest(this.request).subscribe((data: Request) => {
      this.notification.success('Request Created'); //Ativa uma notificação
      //Atualiza a página de listagem de pedidos
      this.data.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.data.router.onSameUrlNavigation = 'reload';
      //Volta para a página de listagem de pedidos
      this.data.router.navigate(['/requests']);
      //Fecha o pop-up
      this.close();
    });
  }

  close() {
    this.dialogRef.close();
  }
}
