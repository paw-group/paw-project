import { Component, OnInit, Input } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { SessionService } from '../../services/session.service';
import { Dashboard } from '../../models/dashboard/dashboard';
import { User } from '../../models/user';
import * as Highcharts from 'highcharts';
import { Router } from '@angular/router';
import HC_exporting from 'highcharts/modules/exporting';
import { CookieService } from 'ngx-cookie-service';

//Testing
import * as fileSaver from 'file-saver';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  //Variaveis usadas no html
  public infected: String = '';
  public not_infected: String = '';
  public suspect: String = '';
  public laboratory: String = '';
  public not_Schedule: String = '';
  public schedule: String = '';
  public done: String = '';
  public missed: String = '';
  public chartOptions = {};
  public Highcharts = Highcharts;

  //Variaveis unicamente usados no componente;
  private user: User;
  private dashboard: Dashboard;
  private days = [];
  private positive = [];

  constructor(
    private dashboardService: DashboardService,
    private session: SessionService,
    private cookieService: CookieService,
    private router: Router,
  ) {
    this.user = new User();
    this.dashboard = new Dashboard();
  }

  ngOnInit() {
    //Fazer setting das informações do dashboard para carregar sem erros
    this.Dashboard();

    // Rediciona para a pagina de login se caso nao estiver logado
    if (this.cookieService.get('user') == '')
      return this.router.navigate(['/login']);

    //Para obter o user que está logado
    this.session.me().subscribe((data: User) => {
      this.user = data;
    });

    //Obter a informação da dashboard
    this.dashboardService.dashboard().subscribe((data: Dashboard) => {
      //Atribuição dos dados da dashboard
      this.dashboard = data;

      //Filtra os dias para se tornar apresentavel no dashboard
      for (let i = 29; i >= 0; i--) {
        this.days.push(
          new Date(this.dashboard.requests[i].date).getDate().toString() +
            '/' +
            (
              new Date(this.dashboard.requests[i].date).getUTCMonth() + 1
            ).toString()
        );
        this.positive.push(this.dashboard.requests[i].PositiveUntilNow);
      }

      //Atualizar as variaveis publicas para mostrar em cards
      this.laboratory = this.dashboard.AllRequests.In_Lab.toString();
      this.infected = this.dashboard.users.Infected.toString();
      this.not_infected = this.dashboard.users.Not_Infected.toString();
      this.suspect = this.dashboard.users.Suspect.toString();

      //Atualizar as variaveis publicas para o admin
      try {
        //Coloquei try só para nao aparecer mensagem de erro no console log pois este erro é esperado
        this.not_Schedule =
          this.dashboard.AllRequests.Not_Schedule.toString() || '';
        this.schedule = this.dashboard.AllRequests.Schedule.toString() || '';
        this.done = this.dashboard.AllRequests.Done.toString() || '';
        this.missed = this.dashboard.AllRequests.Missed.toString() || '';
      } catch (err) {}

      //Atualizar a informação do Dashboard
      this.Dashboard();
    });

    //Para ter a opção de fazer download da imagem do dashboard
    HC_exporting(Highcharts);
  }

  //Fazer setting da informação toda do dashboard
  Dashboard(): void {
    this.chartOptions = {
      chart: {
        type: 'area',
      },
      title: {
        text: 'Progression of confirmed Cases',
      },
      subtitle: {
        text: 'Source: localhost:5000',
      },
      xAxis: {
        categories: this.days,
        tickmarkPlacement: 'on',
        title: {
          enabled: false,
        },
      },
      export: {
        enabled: true,
      },
      yAxis: {
        title: {
          text: ' Cases',
        },
        labels: {
          formatter: function () {
            return this.value;
          },
        },
      },
      tooltip: {
        split: true,
        valueSuffix: ' Cases',
      },
      plotOptions: {
        area: {
          stacking: 'normal',
          lineColor: '#666666',
          lineWidth: 1,
          marker: {
            lineWidth: 1,
            lineColor: '#666666',
          },
        },
      },
      series: [
        {
          name: 'Positive',
          data: this.positive,
        },
      ],
    };
  }

  //Para se é um user normal ou nao
  get userRole() {
    return this.user.role > 0;
  }
}
