import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { RestUsersService } from 'src/app/services/rest-users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { NotificationService } from 'src/app/services/notification.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  user: User;
  stateUsers = ["Suspect", "Not Infected","Infected"];

  private actualUserRole;
  private actualUserId;

  constructor(public rest: RestUsersService,
    private sessionRest: SessionService,
    private notification: NotificationService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.rest.getUser(this.route.snapshot.params['userId']).subscribe((data: User) => {
      this.user = data;

      try {
        this.sessionRest.me().subscribe((data: User) => {
          this.actualUserRole = data.role;
          this.actualUserId = data._id;
          if (this.actualUserId != this.user._id && this.actualUserRole == 0) {
            this.router.navigate(["/"])
          }
        }); //Para ficar com o role da pessoa, assim ja podemos apresentar informação especifica consoante o nivel de acesso concedido
      } catch (err) { this.router.navigate(["/login"]) }
    });

  }

  /**
  * return True se for mais do que um utilizador normal
  */
  isAuthorizedPeople(): boolean {
    return this.actualUserRole > 0;
  }

  /**
   * return True se for mais do que um utilizador normal ou se o perfil for da pessoal logada
   */
  isHimselfOrAuthorizedPeople(): boolean {
    return ((this.actualUserId == this.user._id) || this.isAuthorizedPeople())
  }

  /**
   * return True se for um administrador ou se o perfil for da pessoal logada
   */
  isUserOrAdmin(): boolean {
    return (this.actualUserId == this.user._id || this.actualUserRole == 2)
  }

  listUsers() {
    this.router.navigate(['/user-list'])
  }

  updateUser(id) {
    this.router.navigate(['/user-edit/' + id])
  }

  deleteUser(id) {
    this.rest.deleteUser(id).subscribe(res => {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      if (this.actualUserRole == 2) {
        //Se for admin quando elimina volta para a lista de users
        this.router.navigate(['/user-list']);
      } else {
        //Se for o proprio user logado a eliminar vai para o login
        this.router.navigate(['/login']);
      }
      //Para fechar o dialog
      this.modalService.dismissAll();
    });
    //Mensagem de sucesso
    this.notification.success("User deleted");

    //Correçao de um erro para quando o utilizador se elimina a si proprio ele executa um logout para eleminar os cookies
    if(this.actualUserId == id){
      this.sessionRest.logout().subscribe();
    }
  }

  /**
   * return o estado consoante o id passado
   */
  getStateByID(id) {
    return this.stateUsers[id];
  }

  /**
   * Abre uma Dialog box
   */
  openModal(content) {
    this.modalService.open(content, { size: 'm' });
  }

}
