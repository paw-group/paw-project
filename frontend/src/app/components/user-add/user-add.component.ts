import { Component, OnInit } from '@angular/core';
import { RestUsersService } from 'src/app/services/rest-users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SessionService } from 'src/app/services/session.service';
import { NotificationService } from 'src/app/services/notification.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css'],
})
export class UserAddComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  createTecnic: any;
  actualUserRole: Number;

  users: any;

  constructor(
    public rest: RestUsersService,
    public sessionRest: SessionService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private router: Router,
    private notification: NotificationService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      userName: ['', Validators.required],
      password: ['', Validators.required],
    });

    if (this.cookieService.get('user') != '') {
      this.sessionRest.me().subscribe(
        (data: User) => {
          this.actualUserRole = data.role; //Guarda a role do user logado
          // Se for o admin pode avançar
          if (this.actualUserRole == 2) {
            this.registerForm = this.formBuilder.group({
              name: ['', Validators.required],
              userName: ['', Validators.required],
              password: ['', Validators.required],
              role: undefined,
            });

            this.route.paramMap.subscribe((params) => {
              this.createTecnic = JSON.parse(params.get('labTecnic'));
            });
            //Um user normal que esteja logado não pode avançar por isso vai para a dashboard
          } else {
            this.router.navigate(['/']);
          }
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      //Se não houver ninguem logado podem criar um user normal
      this.registerForm = this.formBuilder.group({
        name: ['', Validators.required],
        userName: ['', Validators.required],
        password: ['', Validators.required],
      });
      this.createTecnic = false;
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit(event): void {
    this.submitted = true;

    // para aqui se nao for valido
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;

    // Se for para criar um tecnico
    if (this.createTecnic == true) {
      this.registerForm.controls['role'].setValue(1);
      this.rest.addTecnic(this.registerForm.value).subscribe(
        (result: User) => {
          this.notification.success('Registration Successful');
          this.router.navigate(['/user-list']);
        },
        (error) => {
          if (error.status == 406) {
            this.notification.warn('Username already exists.');
          } else {
            this.notification.warn(error.statusText);
          }
          this.loading = false;
        }
      );
      //Senao cria um user normal
    } else {
      this.rest.addUser(this.registerForm.value).subscribe(
        (result: User) => {
          this.notification.success('Registration Successful');
          this.router.navigate(['/login']);
        },
        (error) => {
          if (error.status == 406) {
            this.notification.warn('Username already exists.');
          } else {
            this.notification.warn(error.statusText);
          }
          this.loading = false;
        }
      );
    }
  }
}
