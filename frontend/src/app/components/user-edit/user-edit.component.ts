import { Component, OnInit, Input } from '@angular/core';
import { RestUsersService } from '../../services/rest-users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { User } from 'src/app/models/user';
import { NotificationService } from 'src/app/services/notification.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
})
export class UserEditComponent implements OnInit {
  @Input() userData: any = {
    name: '',
    userName: '',
    password: '',
    phoneNumber: '',
    state: '',
  };

  stateUsers = ['Suspect', 'Infected', 'Not Infected'];
  passwordConfirmation = '';
  actualUserRole;
  actualUserId;
  hide1 = true;
  hide2 = true;

  constructor(
    public rest: RestUsersService,
    public sessionRest: SessionService,
    private notification: NotificationService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    try {
      this.sessionRest.me().subscribe((data: User) => {
        this.actualUserRole = data.role;
        this.actualUserId = data._id;
      }); //Para ficar com o role da pessoa, assim ja podemos apresentar informação especifica consoante o nivel de acesso concedido
    } catch (err) {
      this.router.navigate(['/login']);
    }

    this.rest
      .getUser(this.route.snapshot.params['userId'])
      .subscribe((data) => {
        this.userData = data;
        //Para que não sejam alterados
        this.userData.userName = undefined;
        this.userData.password = undefined;
        this.userData.role = undefined;
        this.userData.state = undefined;
      });
  }

  updateUser() {
    let update = true;
    console.log

    //Se não for introduzido nada não atualiza
    if (this.userData.name == '') {
      this.userData.name = undefined;
    }
    if (this.userData.userName == '') {
      this.userData.userName = undefined;
    }
    if (this.userData.password == undefined) {
      this.userData.password = undefined;
    } else if (this.userData.password != this.passwordConfirmation) {
      this.notification.warn("Passwords doesn't match");
      update = false;
    }

    if (
      parseInt(this.userData.phoneNumber) < 910000000 ||
      parseInt(this.userData.phoneNumber) > 969999999
    ) {
      this.notification.warn('Invalid Phone Number');
      update = false;
    }

    if (update) {

      //Traduzir para a base de dados o estados
      if (this.userData.state == 'Suspect') {
        this.userData.state = 0;
      } else if (this.userData.state == 'Infected') {
        this.userData.state = 2;
      } else if(this.userData.state == 'Not Infected'){ //Antigo erro com else mandava sempre o state como 1 ocorrendo um erro em user e atualizando os outros utilizadores
        this.userData.state = 1;
      }

      if (this.actualUserRole == 1) {
        this.userData = { _id: this.userData._id, state: this.userData.state };
      }

      this.rest
        .updateUser(this.route.snapshot.params['userId'], this.userData)
        .subscribe(
          (result) => {
            this.router.navigate(['/user-detail/' + this.userData._id]);
            //Mensagem de sucesso
            this.notification.success('Profile Updated...');
          },
          (err) => {
            if (err.status == 406)
              this.notification.warn('Username is being used');
            else this.notification.warn('Invalid Form');
            console.log(err);
          }
        );
    }

    //Para fechar o dialog
    this.modalService.dismissAll();
  }

  /**
   * Abre uma Dialog box
   */
  openModal(content) {
    this.modalService.open(content, { size: 'm' });
  }
}
