import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { RestRequestsService } from 'src/app/services/rest-requests.service';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  //Variaveis usadas no html
  public autoSchedule: boolean = false;
  public user: User;

  constructor(
    public session: SessionService,
    private requestServide: RestRequestsService,
    private cookieService: CookieService,
    private router: Router
  ) {
    this.user = new User();
  }

  ngOnInit(): void {
    if (this.cookieService.get('user') != '') {
      //Para adquirir as informações do utilizador logado
      this.session.me().subscribe(
        (data: User) => {
          this.user = data;

          // Sempre no inicio do componente faz atribuição do autoSchedule para o butao
          //representar o estado só quando este é um admin senão dá erro de nao autorizado.
          if (data.role == 2) {
            this.requestServide
              .autoSchedule(undefined)
              .subscribe((data: any) => {
                this.autoSchedule = data;
              });
          }
        },
        (err) => {
          //Não é um erro esperado
          console.error(err);
          this.router.navigate(['/login']);
        }
      );
    } else {
      this.router.navigate(['/login']);
    }
  }

  //Ativa e Desativa o autoSchedule dos pedidos
  isAutoSchedule() {
    this.requestServide.autoSchedule(!this.autoSchedule).subscribe(
      (data: any) => {
        this.autoSchedule = data;
      },
      (err) => {
        //Erro nao esperado
        console.error(err);
        this.router.navigate(['/login']);
      }
    );
  }

  //Para obter a confirmação da role de Tecnico no HTML
  get roleTechnic() {
    return this.user.role > 0;
  }

  //Para obter a confirmação da role de Admin no HTML
  get roleAdmin() {
    return this.user.role > 1;
  }

  //Simples logout do utilizador para descartar o cookie com a token
  logout() {
    this.session.logout().subscribe((data: JSON) => {
      this.router.navigate(['/login']);
    });
  }
}
