import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestRequestsService } from '../../services/rest-requests.service';
import { User } from 'src/app/models/user';
import { RestUsersService } from 'src/app/services/rest-users.service';
import { MatDialog } from '@angular/material/dialog';
import { RequestEditComponent } from '../request-edit/request-edit.component';
import { DomSanitizer } from '@angular/platform-browser';
import * as fileSaver from 'file-saver';
import { map } from 'rxjs/operators';
import { SessionService } from 'src/app/services/session.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.component.html',
  styleUrls: ['./request-details.component.css'],
})
export class RequestDetailsComponent implements OnInit {
  request: any;
  stateRequests = ['Not Schedule', 'Schedule', 'In Lab', 'Done', 'Missed'];
  user: User;
  currentUser: User;
  fileUrl;

  constructor(
    public rest: RestRequestsService,
    private route: ActivatedRoute,
    private router: Router,
    public restUsers: RestUsersService,
    public edit: MatDialog,
    private sanitizer: DomSanitizer,
    public session: SessionService,
    public notification: NotificationService
  ) {
    this.user = new User();
    this.currentUser = new User()
  }

  ngOnInit(): void {
    //Guarda o pedido pesquisado
    this.rest
      .getRequest(this.route.snapshot.params['requestId'])
      .subscribe((data: {}) => {
        this.request = data;
        this.restUsers.getUser(this.request.userId).subscribe((data: User) => {
          this.user = data;
        }, (err)=>{
          this.user.userName = "Not Found";
        });
      });

      this.session.me().subscribe((data: User) => {
        this.currentUser = data;
      })
  }

  //Download do pedido
  download() {
    this.rest
      .downloadFile(this.request._id)
      .pipe(map((res) => new Blob([res])))
      .subscribe((res) => {
        let blob: any = res;
        fileSaver.saveAs(blob, 'Exam Results.pdf');
      }),
      (error) => console.log('Error downloading the file'),
      () => console.info('File downloaded successfully');
  }

  //Abre o pop-up de editar
  updateRequest() {
    this.edit.open(RequestEditComponent, {
      data: { id: this.request._id, router: this.router },
    });
  }

  //Elimina o pedido
  deleteRequest() {
    this.rest.deleteRequest(this.request._id).subscribe((res) => {
      this.notification.warn("Request Deleted")
      this.router.navigate(['/requests']);
    });
  }
}
