import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/services/notification.service';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-login-controller',
  templateUrl: './login-controller.component.html',
  styleUrls: ['./login-controller.component.css'],
})
export class LoginControllerComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private session: SessionService,
    private router: Router,
    public notification: NotificationService,
    private cookieService: CookieService,
  ) {}

  ngOnInit(): void {
    // Retornar para o url antigo
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    // Rediciona para a pagina inicial se caso já estiver com o login
    if (this.cookieService.get('user') != '') {
      this.router.navigate([this.returnUrl]);
    }

    //Estabelece um formato para o username e password junto com validações
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  //Faz o controlo das validações, se está com os requesitos minimos para comunicar com o backend
  get f() {
    return this.loginForm.controls;
  }

  handleSubmit(event): void {
    this.submitted = true;

    // Para a submissao se o formulario estiver invalido
    if (this.loginForm.invalid) {
      return;
    }

    //Cancela o comportamento normal de um formulario
    event.preventDefault();

    //Tenta fazer o login
    this.session.login(this.f.username.value, this.f.password.value).subscribe(
      (data) => {
        this.notification.success('Logged in with sucess');
        this.router.navigate([this.returnUrl]);
      },
      //Caso o Erro seja o esperado ele faz o suposto
      (error) => {
        if (error.status == 401) {
          this.loginForm.reset();
          this.submitted = false;
          this.notification.warn('Wrong Credencials');
        } else console.error(error); //Caso nao seja um erro esperado
      }
    );
  }
}
