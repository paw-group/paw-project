import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestRequestsService } from '../../services/rest-requests.service';
import { SessionService } from 'src/app/services/session.service';
import { User } from '../../models/user';
import { MatInput } from '@angular/material/input';
import { RestUsersService } from 'src/app/services/rest-users.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { RequestCreateComponent } from '../request-create/request-create.component';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css'],
})
export class RequestComponent implements OnInit {
  ngAfterViewInit() { }

  requests: any = [];
  stateRequests = ['Not Schedule', 'Schedule', 'In Lab', 'Done', 'Missed'];
  filters = {
    state: 'undefined',
    redirected: 'undefined',
    riskGroup: 'undefined',
    TestDate: 'undefined',
    result: 'undefined',
    user: 'undefined'
  };
  query: String = '';
  user: User;
  users: any = [];
  dataSource = new MatTableDataSource(this.requests);
  displayedColumns = ['ID', 'State', 'User', 'Test Date', 'Actions'];
  currentRequestUser: String;

  constructor(
    public rest: RestRequestsService,
    private route: ActivatedRoute,
    private router: Router,
    public session: SessionService,
    public restUsers: RestUsersService,
    private create: MatDialog,
    public notification: NotificationService
  ) {
    this.user = new User();
  }

  @ViewChild('input', { read: MatInput }) input: MatInput;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit(): void {
    this.getRequests();

    //Guarda a sessão atual
    this.session.me().subscribe((data: User) => {
      this.user = data;

      //Guarda todos os utilizadores
      if (this.user.role != 0) {
        this.restUsers.getUsers().subscribe((data: {}) => {
          this.users = data;
        });
      }
    });
  }

  //Guarda todos os pedidos
  getRequests() {
    this.requests = [];
    this.rest.getRequests(this.query).subscribe((data: {}) => {
      this.requests = data;
      this.updateTable(this.requests);
    });
  }

  createRequest() {
    this.create.open(RequestCreateComponent, { data: { router: this.router } }); //Abre o pop-up de criar pedido
  }

  getRequestDetails(id) {
    this.router.navigate(['/request-details/' + id]); //Reencaminha para a página de detalhes do pedido selecionado
  }

  //Elimina o pedido
  deleteRequest(id) {
    this.rest.deleteRequest(id).subscribe((res) => {
      //Atualiza as informações da página
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/requests']);
      this.updateTable(this.requests);
      this.notification.warn("Request Deleted")
    });
  }

  search() {
    //Criar a query de pesquisa com base nos filtros selecionados
    this.query = '';
    if (this.filters.state != 'undefined') {
      this.query = this.query + '?state=' + this.filters.state;
    }
    if (this.filters.redirected != 'undefined') {
      if (this.query.length != 0) {
        this.query = this.query + '&';
      } else {
        this.query = this.query + '?';
      }
      this.query = this.query + 'redirected=' + this.filters.redirected;
    }
    if (this.filters.riskGroup != 'undefined') {
      if (this.query.length != 0) {
        this.query = this.query + '&';
      } else {
        this.query = this.query + '?';
      }
      this.query = this.query + 'riskGroup=' + this.filters.riskGroup;
    }
    if (this.filters.TestDate != 'undefined') {
      if (this.query.length != 0) {
        this.query = this.query + '&';
      } else {
        this.query = this.query + '?';
      }
      const temp = new Date(this.filters.TestDate);
      var day = '';
      var month = '';

      //Caso o dia ou mês só possua
      if (parseInt(temp.getDate().toString()) < 9) {
        day = '0';
      }
      if (parseInt(temp.getMonth().toString()) < 9) {
        month = '0';
      }

      this.query =
        this.query +
        'TestDate=' +
        temp.getFullYear() +
        '-' +
        month +
        (temp.getMonth() + 1) +
        '-' +
        day +
        temp.getDate();
    }
    if (this.filters.result != 'undefined') {
      if (this.query.length != 0) {
        this.query = this.query + '&';
      } else {
        this.query = this.query + '?';
      }
      this.query = this.query + 'result=' + this.filters.result;
    }
    if (this.filters.user != 'undefined') {
      if (this.query.length != 0) {
        this.query = this.query + '&';
      } else {
        this.query = this.query + '?';
      }
      this.query = this.query + 'userId=' + this.filters.user;
    }

    this.requests = [];
    this.rest.getRequests(this.query).subscribe((data: {}) => {
      this.requests = data;
      this.updateTable(this.requests); //Atualiza a tabela da pedidos
    });
  }

  //Reinicia
  resetDate() {
    this.input.value = '';
    this.filters.TestDate = 'undefined';
  }

  //Atualiza a tabela
  updateTable(data) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  //Retorna o utilizador consoante o seu id
  getUser(id) {
    //Caso só existam pedidos do user
    if (this.user._id == id) {
      this.currentRequestUser = this.user.userName
      return true
    } else {
      this.currentRequestUser = 'Not Found';
      for (let u of this.users) {
        if (u._id == id) {
          this.currentRequestUser = u.userName;
          return true;
        }
      }
    }
    return false;
  }
}
