import { Component, OnInit, Input, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestRequestsService } from '../../services/rest-requests.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NotificationService } from 'src/app/services/notification.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  withCredentials: true,
};
@Component({
  selector: 'app-request-edit',
  templateUrl: './request-edit.component.html',
  styleUrls: ['./request-edit.component.css'],
})
export class RequestEditComponent implements OnInit {
  @Input() request: any = {
    state: '',
    TestDate: '',
    result: '',
    resultFile: '',
  };
  requestId: String;
  stateRequests = ['Not Schedule', 'Schedule', 'In Lab', 'Done', 'Missed'];
  minDate: Date = new Date(Date.now(), Date.now(), Date.now() + 2);

  SERVER_URL = 'http://localhost:5000/api/request/';
  uploadForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public rest: RestRequestsService,
    private route: ActivatedRoute,
    private router: Router,
    public dialogRef: MatDialogRef<Request>,
    public notification: NotificationService,
    private formBuilder: FormBuilder,
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
    //Guarda o pedido selecionado
    this.rest.getRequest(this.data.id).subscribe((request) => {
      this.request = request;
      this.requestId = request._id;
    });

    this.uploadForm = this.formBuilder.group({
      profile: [''],
    });
  }

  //Guarda o pedido
  updateRequest() {
    this.request.userId = undefined; //Correção de erro de enviar o userId sendo que nem existe a opção de o alterar
    this.rest.updateRequest(this.requestId, this.request).subscribe((res) => {
      //Atualiza a página de detalhes
      this.notification.success('Request Edited');
      this.data.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.data.router.onSameUrlNavigation = 'reload';
      //Volta para a página de detalhes
      this.data.router.navigate(['/request-details/' + this.request._id]);
      //Fecha o pop-up
      this.dialogRef.close();
    }, (err) => {

      (err.status==406) ? this.notification.warn("Incorret Data") : console.log(err)
    });
  }


  close(event) {
    this.dialogRef.close();
    event.preventDefault();
  }

  //Upload do ficheiro selecionado
  onSubmit() {
    if (this.uploadForm.get('profile').value != "") {
      const formData = new FormData();
      formData.append('resultFile', this.uploadForm.get('profile').value);
      this.httpClient
        .put<any>(this.SERVER_URL + this.request._id, formData, httpOptions)
        .subscribe(
          (res) => { },
          (err) => console.log(err)
        );
    }
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('profile').setValue(file);
    }
  }
}
