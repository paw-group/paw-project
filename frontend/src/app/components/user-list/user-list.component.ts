import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../models/user';
import { RestUsersService } from 'src/app/services/rest-users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})


export class UserListComponent implements OnInit {

  public isCollapsedAdmin = true; // Para que o menu do admin comece colapsado
  public isCollapsedSearch = true; //Para que o menu de pesquisa comece colapsado
  actualUserRole;
  users: any;
  stateUsers = ["Suspect", "Not Infected","Infected"];
  filters = { "name": "", "phoneNumber": "", "state": "undefined" };

  displayedColumns: string[] = ['users'];
  dataSource = new MatTableDataSource(this.users);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private rest: RestUsersService,
    private sessionRest: SessionService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    try {
      this.sessionRest.me().subscribe((data: User) => {
        this.actualUserRole = data.role;
        // Um utlizador normal não pode aceder á lista de utilizadores
        if (this.actualUserRole == 0) {
          this.router.navigate(["/"]);
        }
      }); //Para ficar com o role da pessoa, assim ja podemos apresentar informação especifica consoante o nivel de acesso concedido
    } // Se não tiver logado vai para o login
    catch (err) { this.router.navigate(["/login"]); }
    this.getUsers();
    this.dataSource.paginator = this.paginator;
  }

  getUsers() {
    this.users = [];
    this.rest.getUsers().subscribe((data: {}) => {
      this.users = data;
      this.updateTable(this.users);
    });
  }

  addUser(labTecnic: boolean) {
    this.router.navigate(['/register/' + labTecnic]);
  }

  search() {
    var query = ""
    var count = 0;

    if (this.filters.name != "") {
      // Como é a primeira query se for usada vai ter sempre o "?" se não for usada nao interfere
      query = query + "?name=" + this.filters.name;
      count++
    }

    if (this.filters.phoneNumber != "") {
      if (count == 0) {
        query = query + "?";
      } else {
        query = query + "&";
      }
      query = query + "phoneNumber=" + this.filters.phoneNumber;
      count++
    }

    if (this.filters.state != "undefined") {
      if (count == 0) {
        query = query + "?";
      } else {
        query = query + "&";
      }
      // Transfrom o state da string para a posicao real no array
      query = query + "state=" + this.stateUsers.findIndex(element => element == this.filters.state);
      count++
    }

    this.users = [];
    this.rest.filterUsers(query).subscribe((data: {}) => {
      this.users = data;
      this.updateTable(this.users);
    });
  }

  updateTable(userList: User[]) {
    this.dataSource = new MatTableDataSource(userList);
    this.dataSource.paginator = this.paginator;
  }
}
