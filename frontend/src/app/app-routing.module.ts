import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginControllerComponent } from './components/login-controller/login-controller.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserAddComponent } from './components/user-add/user-add.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { RequestComponent } from './components/request/request.component';
import { RequestCreateComponent } from './components/request-create/request-create.component';
import { RequestEditComponent } from './components/request-edit/request-edit.component';
import { RequestDetailsComponent } from './components/request-details/request-details.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PrivateAppComponent } from './controllers/private-app/private-app.component';

const routes: Routes = [
  {
    path: '',
    component: PrivateAppComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: 'user-list',
        component: UserListComponent,
      },
      {
        path: 'requests',
        component: RequestComponent,
      },
      {
        path: 'user-detail/:userId',
        component: UserDetailsComponent,
      },
      {
        path: 'register/:labTecnic',
        component: UserAddComponent,
      },
      {
        path: 'user-edit/:userId',
        component: UserEditComponent,
      },
      {
        path: 'request-create',
        component: RequestCreateComponent,
      },
      {
        path: 'request-edit/:requestId',
        component: RequestEditComponent,
      },
      {
        path: 'request-details/:requestId',
        component: RequestDetailsComponent,
      },
    ],
  },
  {
    path: 'login',
    component: LoginControllerComponent,
  },
  {
    path: 'register',
    component: UserAddComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
