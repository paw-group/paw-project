import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { SessionService } from './session.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class RefreshTokenInterceptor implements HttpInterceptor {
  constructor(public auth: SessionService, private router: Router) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    //Para casos em que nao deve fazer refresh do token
    if (request.url.includes('/user/') && request.method == 'POST') {
      //Para casos em que sabemos que o user nao está logado
      return next.handle(request);
    }

    //Para dar refresh no token e durar mais tempo
    this.auth.refresh().subscribe(
      (data) => {},
      (error) => {
        if (error.status == 401) this.router.navigate(['/login']);
        if (error.status == 0) {
          alert('Backend is offline'); //Caso da backend esteja offline
          this.router.navigate(['/login']);
        }
      }
    );

    //Continuar com o pedido
    return next.handle(request);
  }
}
