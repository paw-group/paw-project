import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { Request } from '../models/request';

const endpoint = 'http://localhost:5000/api/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({
  providedIn: 'root',
})
export class RestRequestsService {
  constructor(private http: HttpClient) {}

  private getData(res: Response) {
    var body = res;
    return body || {};
  }

  getRequests(query: String): Observable<Request[]> {
    return this.http.get<Request[]>(endpoint + 'requests' + query, httpOptions);
  }

  getRequest(id: String): Observable<Request> {
    return this.http.get<Request>(endpoint + 'request/' + id, httpOptions);
  }

  createRequest(request: Request): Observable<Request> {
    return this.http.post<Request>(
      endpoint + 'request' + '?autoSchedule=true',
      JSON.stringify(request),
      httpOptions
    );
  }

  updateRequest(id: String, request: Request): Observable<Request> {
    return this.http.put<Request>(
      endpoint + 'request/' + id,
      JSON.stringify(request),
      httpOptions
    );
  }

  deleteRequest(id: String): Observable<Request> {
    return this.http.delete<Request>(endpoint + 'request/' + id, httpOptions);
  }

  autoSchedule(AUTOSCHEDULE: boolean): Observable<any> {
    return this.http.put<any>(
      endpoint + 'request/autoSchedule',
      { AUTOSCHEDULE: AUTOSCHEDULE },
      httpOptions
    );
  }

  downloadFile(id: String): Observable<any> {
    return this.http.get(endpoint + 'request/file/' + id, {
      responseType: 'arraybuffer',
      withCredentials: true,
    });
  }
}
