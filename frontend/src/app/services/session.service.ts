import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { map } from 'rxjs/operators'; para ver o seu uso mais tarde
import { CookieService } from 'ngx-cookie-service';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

const endpoint = 'http://localhost:5000/api/user/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  withCredentials: true, //Para fazer o setting dos cookies
};

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private router: Router
  ) {}

  login(username, password) {
    return this.http.post(
      endpoint + 'login',
      { userName: username, password: password },
      httpOptions
    );
  }

  refresh(): Observable<any> {
    return this.http.post(endpoint + 'refresh', {}, httpOptions);
  }

  me(): Observable<User> {
    //Caso nao esteja logado direciona automaticamente para o login
    if(this.cookieService.get('user') == ''){
      this.logout();
      this.router.navigate(['/login']);
    }

    return this.http.get<User>(
      endpoint + this.cookieService.get('user'),
      httpOptions
    );
  }

  logout() {
    return this.http.post(endpoint + 'logout', {}, httpOptions);
  }
}
