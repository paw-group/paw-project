import { TestBed } from '@angular/core/testing';

import { RefreshTokenInterceptor } from './token-interceptor.service';

describe('RefreshTokenInterceptorService', () => {
  let service: RefreshTokenInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RefreshTokenInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
