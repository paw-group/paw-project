import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';

const endpoint = 'http://localhost:5000/api/user';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  withCredentials: true, //Para fazer o setting dos cookies
};

@Injectable({
  providedIn: 'root'
})
export class RestUsersService {

  constructor(private http: HttpClient) { }

  getUser(id: String): Observable<User> {
    return this.http.get<User>(endpoint + '/' + id, httpOptions);
  }

  getUsers(): Observable<any> {
    return this.http.get<any>(endpoint + '/', httpOptions);
  }

  filterUsers(query: String): Observable<any> {
    return this.http.get<any>(endpoint + '/' + query, httpOptions)
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(endpoint + '/', JSON.stringify(user), httpOptions);
  }

  addTecnic(user: User): Observable<User> {
    return this.http.post<User>(endpoint + '/technicLab/', JSON.stringify(user), httpOptions);
  }

  updateUser(id: string, user: User): Observable<User> {
    return this.http.put<User>(endpoint + '/' + id, JSON.stringify(user), httpOptions);
  }

  deleteUser(id: string): Observable<User> {
    return this.http.delete<User>(endpoint + '/' + id, httpOptions);
  }
}
