import { DashboardRequests } from './requests';
import { DashboardUser } from './users';

export class Dashboard {
  users: DashboardUser;
  AllRequests: DashboardRequests;
  requests: DashboardRequests[];
}
