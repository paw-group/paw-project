export class DashboardRequests {
  Positive: Number;
  Negative: Number;
  PositiveUntilNow: Number;
  NegativeUntilNow: Number;
  Done: Number;
  Missed: Number;
  In_Lab: Number;
  Schedule: Number;
  Not_Schedule: Number;
  date: Date;
}
