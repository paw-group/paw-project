export class User {
  _id : String;
  name: String;
  userName: String;
  password: String;
  role: Number;
  phoneNumber: Number;
  state: Number;
  updated_at: Date;
 }
