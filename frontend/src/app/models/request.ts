export class Request {
    _id: String
    state: number
    userId: String
    TestDate: Date
    redirected: Boolean
    riskGroup: Boolean
    resultFile: String 
    result:  Boolean
    updated_at: Date
}